# asm8080 version 1.0.12

This project is a fork of the Intel 8080 Assembler by Jay Cotton
and Claude Sylvain.

http://asm8080.sourceforge.net/

It was forked from the version 1.0.12 (from 14 September 2013). There are no
functional changes in the fork.
